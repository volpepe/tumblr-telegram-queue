from flask import Flask, request
import telegram
from telegram import InputMediaPhoto, InputMediaAnimation, ParseMode
import pytumblr
import time

from telegram.files.animation import Animation

from safe.credentials import bot_token, tumblr_consumer_key, channel_id
from tumblr_posts_queue import PostQueue, DefaultScheduler
from utils.text_gen import default_message_func, hi_bot_text, pause_bot_text, \
                                                 stop_bot_text, dont_bother_bot_text

app = Flask(__name__)
bot = telegram.Bot(token=bot_token)
pq = None
scheduler = None
update_post_num_scheduler = None
index = 0
update_time = 60*60*2 # 2 hours

"""
This is a handler for the PostQueue that sends the message whenever it's ready
"""
def send_post(msg):
    print("Posting message: {}".format(str(msg)))
    caption = "<a href='{}'>{}</a>".format(msg.link, msg.text)
    parse_mode = ParseMode.HTML
    if len(msg.photos) > 1:
        # Multiple photos or gifs
        media = []
        gifs = []
        sent_messages = []
        for photo in msg.photos:
            # Check if GIF or photo
            if photo[-3:] == 'gif':
                gifs.append(photo)
            else: 
                media.append(InputMediaPhoto(
                                photo, 
                                caption = caption,
                                parse_mode = parse_mode), 
                            )
        if len(media) > 0:
            sent_messages = bot.send_media_group(chat_id = channel_id,
                                                media = media,
                                                allow_sending_without_reply = True)
        # Deal with gifs
        if len(gifs) > 0:
            if len(sent_messages) > 0:
                # If some of the photos were actual photos and sent as such:
                reply_id = sent_messages[0].message_id
            else:
                # Send the first gif, get its message id
                first_sent_gif = bot.send_animation(chat_id = channel_id,
                                                animation = gifs[0],
                                                caption = caption,
                                                parse_mode = parse_mode,
                                                allow_sending_without_reply = True)
                reply_id = first_sent_gif.message_id
                # Update the gifs array (this operation is always fine because if there is only a gif
                # it will return an empty list and the for loop would not be executed):
                gifs = gifs[1:]
            for gif in gifs:
                # Wait 2 seconds between each gif to avoid hitting limits
                time.sleep(2)
                bot.send_animation(chat_id = channel_id,
                                    animation = gif,
                                    caption = caption,
                                    parse_mode = parse_mode,
                                    reply_to_message_id = reply_id)
    else:
        # Single photo or gif
        if msg.photos[0][-3:] == 'gif':
            bot.send_animation(chat_id = channel_id,
                                animation = msg.photos[0],
                                caption = caption,
                                parse_mode = parse_mode,
                                allow_sending_without_reply = True)
        else:
            bot.send_photo(chat_id = channel_id,
                            photo = msg.photos[0],
                            caption = caption,
                            parse_mode = parse_mode,
                            allow_sending_without_reply = True)

@app.route('/')
def index():
    return '.'

if __name__ == '__main__':
    # Get last index
    with open("index.txt", 'r') as f:
        index = int(f.readlines()[0])
    print("Starting from index {}".format(index))
    # Authenticate via API Key
    client = pytumblr.TumblrRestClient(tumblr_consumer_key)
    # 4 hours between each message
    pq = PostQueue( client,
                    msg_handler=send_post, 
                    max_queue_size=50,
                    message_func=default_message_func,
                    restart_from_offset=index,
                    index_file="index.txt")
    scheduler = DefaultScheduler(update_time, pq.get_new_post)    # 2 hours
    update_post_num_scheduler = DefaultScheduler(86400, pq.update_posts_num) # 1 day
    bot.send_message(chat_id=channel_id, text=hi_bot_text)
    pq.start()
    update_post_num_scheduler.start()
    scheduler.start()
    app.run(threaded=True)
