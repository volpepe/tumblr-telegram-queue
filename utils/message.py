class Message():
    def __init__(self, photos, link, text):
        self.photos = photos
        self.link = link
        self.text = text

    def __str__(self) -> str:
        return str({'photos': self.photos, 
                    "link": self.link,
                    "text": self.text})

def fake_handler(msg):
    print("Created Message {}".format(str(msg)))