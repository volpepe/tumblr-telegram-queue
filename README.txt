To run on your own blogs you need to ask Tumblr permission to use the API (see here https://www.tumblr.com/docs/en/api/v2).
You also need to create your own bot on Telegram with Bot Father (see here: https://core.telegram.org/bots).
Finally, create a channel for your bot to send images in.

Compile safe/credentials.py using the keys provided by Tumblr and Bot Father.
Finally, create an environment using venv (python -m venv <env-name>), activate it (source <env-name>/bin/activate or <env-name>\Scripts\activate.ps1 on Powershell) and install the dependencies with pip (python -m pip install -r requirements.txt).

Finally, run app.py with python app.py.