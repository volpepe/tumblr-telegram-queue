import pytumblr
from safe.credentials import tumblr_consumer_key, blog_url
from utils.message import Message, fake_handler
from utils.text_gen import default_message_func
import atexit
from apscheduler.schedulers.background import BackgroundScheduler

from datetime import datetime

class EndOfQueueException(Exception):
    """Raised when Tumblr post queue has been exhausted"""
    pass

class DefaultScheduler():
    def __init__(self, s_between_posts, post_func):
        self.s_between_posts = s_between_posts
        self.post_func = post_func
        self.scheduler = BackgroundScheduler()
        self.scheduler.add_job(func=post_func, 
                                trigger='interval', 
                                seconds=s_between_posts)
        atexit.register(lambda: self.scheduler.shutdown())

    def start(self):
        self.scheduler.start()
            
class PostQueue():
    def __init__(self, client, msg_handler,
                max_queue_size=50, 
                message_func=default_message_func,
                restart_from_offset=None,
                index_file=None):
        super(PostQueue, self).__init__()
        # Max queue size can be changed, but tumblr does not allow more than 50 posts per request.
        self.max_queue_size = max_queue_size
        # Authenticated client
        self.client = client
        # Message handler
        self.msg_handler = msg_handler
        # Get total number of posts to control
        self.posts_num = self.client.blog_info(blog_url)['blog']['posts']
        # Message function that creates the text to send as a message
        # Message function should always take as argument a post dict.
        self.message_func = message_func
        # QUEUE VARIABLES
        self.checked_posts = 0
        
        # self.current_offset = int(self.max_queue_size * (self.posts_num // self.max_queue_size))
        self.current_offset = int(self.posts_num - self.max_queue_size + 1)
        self.started_from = 0
        if restart_from_offset is not None:
            self.current_offset -= restart_from_offset
            self.started_from = restart_from_offset
        self.current_post_queue = []
        self.current_queue_index = 0
        self.index_file = index_file

        print("This blog has {} posts. Initializing queue...".format(self.posts_num))

    def start(self):
        self.current_post_queue = self.__queue_update()
        self.update_posts_num()
        self.get_new_post()

    def print_current_server_time(self):
        current_time = datetime.now().strftime("%H:%M:%S")
        print("Current Time =", current_time)

    def update_posts_num(self):
        # Run periodically to update the number of posts available
        posts_num = self.client.blog_info(blog_url)['blog']['posts']
        self.print_current_server_time()
        print("Updated posts num: {}".format(posts_num))
        # We need to update the current offset variable adding the difference
        # Implication: posts number can only grow
        self.current_offset = self.current_offset + (posts_num - self.posts_num)
        # Do the update and also return the number
        self.posts_num = posts_num
        # Print stats
        self.print_status()
        return posts_num
    
    def __queue_update(self):
        if self.checked_posts < self.posts_num:
            # Drop a 50 post request
            self.print_current_server_time()
            print("========================")
            print("Requesting more posts...")
            print("========================")
            current_post_queue = self.client.posts(blog_url, limit = self.max_queue_size, 
                                                            offset = self.current_offset)
            # Print stats
            self.print_status()
            # Iterate through the list in reverse order so that older posts 
            # are checked before new ones
            return current_post_queue['posts'][::-1]
        return None
        
    def get_new_post(self):
        # Retrieve posts until they are photo posts or queue ends
        photo_post = False
        while not photo_post:
            # Get post from queue
            post = self.current_post_queue[self.current_queue_index]
            self.current_queue_index += 1
            self.checked_posts += 1
            # If queue index was the last one for the queue, request a new queue
            if self.current_queue_index >= len(self.current_post_queue):
                # Check if the post num has changed. 
                # This may update both the current offset and the posts num.
                self.update_posts_num()
                # Update offset for next request
                self.current_offset -= self.max_queue_size
                # Update queue with new offset
                self.current_post_queue = self.__queue_update()
                self.current_queue_index = 0
                # What if no posts could be retrieved? Raise EndOfQueueException.
                if self.current_post_queue is None:
                    raise EndOfQueueException()
            
            # Modify the index file
            if self.index_file is not None:
                with open(self.index_file, 'w') as f:
                    f.write(str(self.started_from+self.checked_posts))

            # Update photo_post variable and exit the loop if it's fine.
            photo_post = self.__check_post(post)

            if not photo_post:
                self.print_current_server_time()
                print("Not a photo post. Skipped a message.")

        # At this point, we have a photo post
        # Modify the arguments
        text = self.message_func(post)
        msg = self.__create_message(photo_post, text)
        # Send message to the handler
        self.msg_handler(msg)

        # Print status:
        self.print_status()

    def __check_post(self, post):
        if post['type'] == 'photo':
            photos = [img['original_size']['url'] for img in post['photos']]
            try:
                link = post['source_url']
            except KeyError:
                link = post['short_url']
            return {
                'photos': photos,
                'link': link
            }
        return False

    def __create_message(self, checked_post, text):
        return Message( checked_post['photos'], 
                        checked_post['link'], 
                        text)
                    
    def print_status(self):
        self.print_current_server_time()
        print("====== SESSION STATS ======")
        print("Total posts num: {}".format(self.posts_num))
        print("Current offset: {}".format(self.current_offset))
        print("Current queue index: {}".format(self.current_queue_index))
        print("Checked posts from startup: {}".format(self.checked_posts))
        print("Session started from index: {}".format(self.started_from))
        print("===========================")

if __name__ == "__main__":
    # Authenticate via API Key
    client = pytumblr.TumblrRestClient(tumblr_consumer_key)
    # Setup PostQueue and scheduler
    pq = PostQueue(client, fake_handler)
    scheduler = DefaultScheduler(5, pq.get_new_post)
    pq.start()
    scheduler.start()
    # Main Loop that will be replaced by Flask main loop
    try:
        while True:
            pass
    except (KeyboardInterrupt, EndOfQueueException) as e:
        print(e)